module.exports.profileEmbed = function(client, message, user, iUser, Discord) {
    var embed = new Discord.RichEmbed()
        .setTitle(user.username)
        .setDescription(`**Level:** ${iUser.uLevel} \n**Exp:** ${iUser.globalPoints} / ${iUser.nextPL}\n**Rank:** ${iUser.globalRank}`)
        .setColor(0xff00d6)
        .setThumbnail(user.displayAvatarURL)
        .setFooter("There will be special roles on reward. Keep chatting!", "http://i68.tinypic.com/21a0ppx.gif");
        
    message.channel.send({embed: embed});

};