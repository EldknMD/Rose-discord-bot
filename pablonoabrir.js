const talkedRecently = new Set();
exports.run = async (client, message, args, sql, Discord) => {
    if (talkedRecently.has(message.author.id)) {
        message.channel.send("I'm feeling bad, onii-chan, let me rest for 30 seconds :3 - " + message.author);
    } else {
		var level = await(async function(){
			try{
				const member = message.guild.member(message.mentions.users.first());
				if(!member){
					const iUser = await sql.get(`SELECT * FROM userScores WHERE guildID = '${message.guild.id}' AND userID = '${member? member.id : message.author.id}'`); //gets user row of whos requesting
					if(!iUser){
						return 1;
					}else{
						return iUser.uLevel;
					}
				}
			} catch (e){
				console.log("Exception occured", e);
				return 1;
			}
		}(message));
		if(level && level < 1) {
			level = 1;
		}
		console.log("Level is:: "+level);
		const weightedRandom = function(level){
			var result = 0;
			for (let i=0; i<=level; i++){
				result = Math.max(Math.random(), result);
			}
			return result;
		}
        var fs = require("fs");
        var alturarose = fs.readFileSync("alturarose.txt", "utf8");
        var bustModifier = parseFloat(fs.readFileSync("bustModifier.txt", "utf8"));
        var numberalturarose = Number(alturarose);
        var adicion = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 5, 5, 100];
		var bustBonus = weightedRandom(level) > 0.930;
		if(bustBonus){
			bustModifier += 0.0045;
		}
        var adicionrandom = Math.floor(weightedRandom(level) * adicion.length);
        var adiciontotal = adicion[adicionrandom];
        var alturanuevarose = numberalturarose + adiciontotal;
        var bustorose = Math.round((alturanuevarose * bustModifier)) / 100;
        var cinturarose = Math.round((alturanuevarose * 0.4)) / 100;
        var caderarose = Math.round((alturanuevarose * 0.5)) / 100;
        var pesobustorose = Math.round(bustorose * 10);
        var alturaroseenmetros1 = numberalturarose / 100;
        var alturaroseenmetros2 = alturanuevarose / 100;
		var weight = Math.round(alturanuevarose * alturanuevarose * alturanuevarose * 0.00001091);
        var role = message.guild.roles.find(role => role.name === "Growth master");
        fs.writeFile("alturarose.txt", alturanuevarose, function(err) {
            if (err) return console.log(err);
        });
        fs.writeFile("bustModifier.txt", bustModifier, function(err) {
            if (err) return console.log(err);
        });
        var facts = ["Mmmn... this feels strange...",
            "You're weird...",
            "Hey... i'm getting bigger",
            "My clothes are ripped...",
            "Whoa... this doesn't feel bad at all",
            "Be careful around me... i don't wanna hurt you",
            "I feel dizzy...",
            "Don't look at me!",
            "Are you happy now?",
            "I thought dynamax only worked on pokemon",
            "The serum is so powerful..."
        ];
        var fact = Math.floor(Math.random() * facts.length);
        var embed = new Discord.RichEmbed()
            .setTitle(alturaroseenmetros1 + " Meters" + "->" + alturaroseenmetros2 + " Meters")
            .setDescription(facts[fact])
            .setColor(0xff00d6)
            .addField("**Bust =" + bustorose + " Meters" + ", Waist =" + cinturarose + " Meters" + ", Hips=" + caderarose + " Meters, Weight=" + weight +" KG**", "I'm huge :3")
            .setThumbnail(client.user.displayAvatarURL);
        message.channel.send({
            embed
        });
        if (adiciontotal > 99) {
            message.member.addRole(role);
            message.channel.send(message.author + " you're a growth master, honey!");
        }
		if(bustBonus) {
            message.channel.send(message.author + " That one seemed to go mostly to my chest.");
		}
    }
    talkedRecently.add(message.author.id);
    setTimeout(() => {
        // Removes the user from the set after a minute
        talkedRecently.delete(message.author.id);
    }, 30000);
};