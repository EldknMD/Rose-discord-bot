const growthCooldown = {};
exports.run = async (client, message, args, sql, Discord) => {
	const timeDiff = growthCooldown[message.author.id] - new Date().getTime();
	if(timeDiff > 3600000){ //More than an hour left.
		const hours = Math.round((timeDiff /360000)) / 10
        message.channel.send("I'm feeling bad :c, let me rest for... maybe " + hours + " hours? :3 - " + message.author);
	} else if (timeDiff > 0) {
		const minutes = Math.round((timeDiff /6000)) / 10
        message.channel.send("I'm feeling bad :c, let me rest for... maybe " + minutes + " minutes? :3 - " + message.author);
    } else {
		var level = await(async function(){
			try{
				const member = message.guild.member(message.mentions.users.first());
				if(!member){
					const iUser = await sql.get(`SELECT * FROM userScores WHERE guildID = '${message.guild.id}' AND userID = '${member? member.id : message.author.id}'`); //gets user row of whos requesting
					if(!iUser){
						return 1;
					}else{
						return iUser.uLevel;
					}
				}
			} catch (e){
				console.log("Exception occured", e);
				return 1;
			}
		}(message));
		if(level && level < 1) {
			level = 1;
		}
		console.log("Level is:: "+level);
		const weightedRandom = function(level){
			var result = 0;
			for (let i=0; i<=level; i++){
				result = Math.max(Math.random(), result);
			}
			return result;
		}
        var fs = require("fs");
        var alturarose = parseFloat(fs.readFileSync("alturarose.txt", "utf8"));
        var bustModifier = parseFloat(fs.readFileSync("bustModifier.txt", "utf8"));
        var waistModifier = parseFloat(fs.readFileSync("waistModifier.txt", "utf8"));
        var hipModifier = parseFloat(fs.readFileSync("hipModifier.txt", "utf8"));
        var numberalturarose = Number(alturarose);
        var adicion = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 4, 4, 4, 5, 5, 100];
		var bustBonus = weightedRandom(level) > 0.930;
        var adicionrandom = Math.floor(weightedRandom(level) * adicion.length);
        var adiciontotal = adicion[adicionrandom];
        var alturanuevarose = numberalturarose + adiciontotal;
        var bustoantiguorose = Math.round((alturarose * bustModifier)) / 100;
        var cinturaantiguarose = Math.round((alturarose * waistModifier)) / 100;
        var caderaantiguarose = Math.round((alturarose * hipModifier)) / 100;
		if(bustBonus){
			bustModifier += 0.0045;
		}
        var bustorose = Math.round((alturanuevarose * bustModifier)) / 100;
        var cinturarose = Math.round((alturanuevarose * waistModifier)) / 100;
        var caderarose = Math.round((alturanuevarose * hipModifier)) / 100;
        var pesobustorose = Math.round(bustorose * 10);
        var alturaroseenmetros1 = numberalturarose / 100;
        var alturaroseenmetros2 = alturanuevarose / 100;
		var weight = Math.round(21.4 * (alturanuevarose/100) * (alturanuevarose/100));
        var role = message.guild.roles.find(role => role.name === "Growth master");
        fs.writeFile("alturarose.txt", alturanuevarose, function(err) {
            if (err) return console.log(err);
        });
        fs.writeFile("bustModifier.txt", bustModifier, function(err) {
            if (err) return console.log(err);
        });
        var facts = ["Ah... I loved that bra :c",
            "I don't believe this is a good idea",
            "I'll do anything for onii-chan, even this",
            "I don't want to be a MONSTER :C",
            "Mmmnn... this feeling is... weird.",
            "I feel a big one coming...",
            "Augh, my head...",
            "Don't you dare to look at my with those eyes!",
            "I feel weird... like I'm gonna explode...",
            "GIGANTAMAX is better...",
            "Non-linear growth spurts, Night? Does it looks like a good idea on a server like this?"
        ];
        var fact = Math.floor(Math.random() * facts.length);
        var embed = new Discord.RichEmbed()
            .setTitle(alturaroseenmetros1 + " Meters" + "->" + alturaroseenmetros2 + " Meters")
            .setDescription(facts[fact])
            .setColor(0xff00d6)
            .addField("**Bust =**", bustoantiguorose + " Meters" + "->" + bustorose + " Meters")
            .addField( "**Waist =**", cinturaantiguarose + " Meters" + "->" + cinturarose + " Meters")
            .addField("**Hips=**", caderaantiguarose + " Meters" + "->" + caderarose + " Meters")
            .setThumbnail(client.user.displayAvatarURL);
        message.channel.send({
            embed
        });
        if (adiciontotal > 99) {
            message.member.addRole(role);
            message.channel.send(message.author + " you're a growth master, honey!");
        }
		if(bustBonus) {
            message.channel.send(message.author + " That one seemed to go mostly to my chest.");
		}
		growthCooldown[message.author.id] = new Date().getTime() + 14400000;
    }
};