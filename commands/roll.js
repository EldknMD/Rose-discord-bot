const growthCooldown = {};
exports.run = (client, message, args, sql, Discord) => {
    var fs = require("fs");
    var dice = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20];
    var faceup = Math.floor(Math.random() * dice.length);
    var result = (dice[faceup]);
    var role = message.guild.roles.find(role => role.name === "Growth master");
    const timeDiff = growthCooldown[message.author.id] - new Date().getTime();
    if (timeDiff > 3600000) { //More than an hour left.
        const hours = Math.round((timeDiff / 360000)) / 10;
        message.channel.send("I'm feeling bad :c, let me rest for... maybe " + hours + " hours? :3 - " + message.author);
    } else if (timeDiff > 0) {
        const minutes = Math.round((timeDiff / 6000)) / 10;
        message.channel.send("I'm feeling bad :c, let me rest for... maybe " + minutes + " minutes? :3 - " + message.author);
    } else {
		let text = "";
        let hip = parseFloat(fs.readFileSync("hipModifier.txt", "utf8"));
		let bustModifier = parseFloat(fs.readFileSync("bustModifier.txt", "utf8"));
        let height = parseFloat(fs.readFileSync("alturarose.txt", "utf8"));
        let waistModifier = parseFloat(fs.readFileSync("waistModifier.txt", "utf8"));
		let hipAdition = hip;
		let heightAdition = height;
		let bustAdition = bustModifier;
        if (result == 1) {
            text = "Nothing happens...";
        }
        if (result == 2) {
            text = "little bust boost";
            bustAdition = bustModifier + 0.002;
            fs.writeFile("bustModifier.txt", bustAdition, function(err) {
                if (err) return console.log(err);
            });
        }
        if (result == 3) {
            text = "little hip boost";
            hipAdition = hip + 0.002;
            fs.writeFile("hipModifier.txt", hipAdition, function(err) {
                if (err) return console.log(err);
            });
        }
        if (result == 4) {
            text = "little height boost";
            heightAdition = height + 2;
            fs.writeFile("alturarose.txt", heightAdition, function(err) {
                if (err) return console.log(err);
            });
        }
        if (result == 5) {
            text = "normal bust boost";
            bustAdition = bustModifier + 0.003;
            fs.writeFile("bustModifier.txt", bustAdition, function(err) {
                if (err) return console.log(err);
            });
        }
        if (result == 6) {
            text = "normal hip boost";
            hipAdition = hip + 0.003;
            fs.writeFile("hipModifier.txt", hipAdition, function(err) {
                if (err) return console.log(err);
            });

        }
        if (result == 7) {
            text = "normal height boost";
            heightAdition = height + 3;
            fs.writeFile("alturarose.txt", heightAdition, function(err) {
                if (err) return console.log(err);
            });
        }
        if (result == 8) {
            text = "big bust boost";
            bustAdition = bustModifier + 0.004;
            fs.writeFile("bustModifier.txt", bustAdition, function(err) {
                if (err) return console.log(err);
            });
        }
        if (result == 9) {
            text = "big hip boost";
            hipAdition = hip + 0.004;
            fs.writeFile("hipModifier.txt", hipAdition, function(err) {
                if (err) return console.log(err);
            });

        }
        if (result == 10) {
            text = "big height boost";
            heightAdition = height + 4;
            fs.writeFile("alturarose.txt", heightAdition, function(err) {
                if (err) return console.log(err);
            });
        }
        if (result == 11) {
            text = "mega bust boost";
            bustAdition = bustModifier + 0.006;
            fs.writeFile("bustModifier.txt", bustAdition, function(err) {
                if (err) return console.log(err);
            });
        }
        if (result == 12) {
            text = "mega hip boost";
            hipAdition = hip + 0.006;
            fs.writeFile("hipModifier.txt", hipAdition, function(err) {
                if (err) return console.log(err);
            });
        }
        if (result == 13) {
            text = "mega height boost";
            heightAdition = height + 6;
            fs.writeFile("alturarose.txt", heightAdition, function(err) {
                if (err) return console.log(err);
            });
        }
        if (result == 14) {
            text = "giga bust boost";
            bustAdition = bustModifier + 0.008;
            fs.writeFile("bustModifier.txt", bustAdition, function(err) {
                if (err) return console.log(err);
            });
        }
        if (result == 15) {
            text = "giga hip boost";
            hipAdition = hip + 0.008;
            fs.writeFile("hipModifier.txt", hipAdition, function(err) {
                if (err) return console.log(err);
            });
        }
        if (result == 16) {
            text = "giga height boost";
            heightAdition = height + 8;
            fs.writeFile("alturarose.txt", heightAdition, function(err) {
                if (err) return console.log(err);
            });
        }
        if (result == 17) {
            text = "tera bust boost";
            bustAdition = bustModifier + 0.01;
            fs.writeFile("bustModifier.txt", bustAdition, function(err) {
                if (err) return console.log(err);
            });
        }
        if (result == 18) {
            text = "tera hip boost";
            hipAdition = hip + 0.01;
            fs.writeFile("hipModifier.txt", hipAdition, function(err) {
                if (err) return console.log(err);
            });
        }
        if (result == 19) {
            text = "tera height boost";
            heightAdition = height + 10;
            fs.writeFile("alturarose.txt", heightAdition, function(err) {
                if (err) return console.log(err);
            });
        }
        if (result == 20) {
            text = "tera ALL boost";
            heightAdition = height + 10;
            fs.writeFile("alturarose.txt", heightAdition, function(err) {
                if (err) return console.log(err);
            });
            hipAdition = hip + 0.01;
            fs.writeFile("hipModifier.txt", hipAdition, function(err) {
                if (err) return console.log(err);
            });
            bustAdition = bustModifier + 0.01;
            fs.writeFile("bustModifier.txt", bustAdition, function(err) {
                if (err) return console.log(err);
            });
            message.member.addRole(role);
            message.channel.send(message.author + " you're a growth master too, honey!");
        }
        growthCooldown[message.author.id] = new Date().getTime() + 3600000;

		
        var startHeight = Math.round(height) / 100;
        var endHeight = Math.round(heightAdition) / 100;
        var startBust = Math.round((height * bustModifier)) / 100;
        var endBust = Math.round((heightAdition * bustAdition)) / 100;
        var startHips = Math.round((height * hip)) / 100;
        var endHips = Math.round((heightAdition * hipAdition)) / 100;
        var startWaist = Math.round((height * waistModifier)) / 100;
        var endWaist = Math.round((heightAdition * waistModifier)) / 100;
		var weight = Math.round(21.4 * (heightAdition/100) * (heightAdition/100));

        var embed = new Discord.RichEmbed()
            .setTitle(startHeight + " Meters" + "->" + endHeight + " Meters")
            .setDescription(text)
            .setColor(0xff00d6)
            .addField("**Bust =**", startBust + " Meters" + "->" + endBust + " Meters")
            .addField("**Waist =**", startWaist + " Meters" + "->" + endWaist + " Meters")
            .addField("**Hips=**", startHips + " Meters" + "->" + endHips + " Meters")
            .setThumbnail(client.user.displayAvatarURL);
        message.channel.send({embed});
    }
};