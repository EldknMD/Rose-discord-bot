const pEmbed = require('./../embeds/eProfile.js');
exports.run = (client, message, args, sql, Discord) =>{
    const member = message.guild.member(message.mentions.users.first());

    if(!member){
        sql.get(`SELECT * FROM userScores WHERE guildID = '${message.guild.id}' AND userID = '${message.author.id}'`).then(iUser =>{ //gets user row of whos requesting
            if(!iUser){
                message.reply("Sorry, no points :c. Start chatting to earn them!");
            }else{
                pEmbed.profileEmbed(client, message, message.author, iUser, Discord);
            }
        });
    }else{
        sql.get(`SELECT * FROM userScores WHERE guildID = '${message.guild.id}' AND userID = '${member.id}'`).then(iUser =>{
            if(!iUser){
                message.reply("Sorry no points :c. Have them start chatting to earn them!");
            }else{
                pEmbed.profileEmbed(client, message, member.user, iUser, Discord);
            }
        });
    }
};